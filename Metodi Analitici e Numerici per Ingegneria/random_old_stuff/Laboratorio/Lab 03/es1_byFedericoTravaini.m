%% Esercizio 1
clear; clc;
%% Punto 1
n = 16;
A = 4 * diag(ones(n, 1))   ...
  - diag(ones(n-1, 1), 1)  ...
  - diag(ones(n-2, 1), 2)  ...
  - diag(ones(n-1, 1), -1) ...
  - diag(ones(n-2, 1), -2);
b = 0.2 * ones(n, 1);
if (A == transpose(A))
  disp('Simmetrica'); 
else 
  disp('Non Simmetrica'); 
end
D = eig(A);
if (D > 0)
  disp('Definita Positiva'); 
else 
  disp('Non Definita Positiva'); 
end
%% Punto 3
[x, niter, res, incr] = gradiente(A, b, zeros(n, 1), 500, 1e-5);
%% Punto 4
semilogy (1:niter, incr, 1:niter, res);
grid on;
xlabel ('Iterazioni');
legend ('Incrementi', 'Residui');
%% Punto 5
x = pcg(A, b, 1e-5, 500);
%% Punto 6
clear; clc;
N    = [ 16, 32, 64, 128, 256 ];
toll = 1e-5;
maxiter = 100000;
NG  = [];
NCG = [];
COND = [];
for n = N
  A = 4 * diag(ones(n, 1))   ...
    - diag(ones(n-1, 1), 1)  ...
    - diag(ones(n-2, 1), 2)  ...
    - diag(ones(n-1, 1), -1) ...
    - diag(ones(n-2, 1), -2);
  b = 0.2 * ones(n, 1);
  x0 = zeros(n, 1);
  [x, ng]  = gradiente(A, b, x0, maxiter, toll);
  [x, stat, res, ncg] = pcg(A, b, toll, maxiter);
  NG  = [ NG,  ng  ];
  NCG = [ NCG, ncg ];
  COND = [ COND, cond(A) ];
end
loglog (N, NG, 'bo-', N, NCG, 'ro-',  N, COND, 'k-', N, sqrt(COND), 'k--');
grid on;
xlabel ('n');
ylabel ('Iterazioni');
legend ('Gradiente', 'G. Coniugato', 'K(A)','K(A)^{1/2}','Location','best');
