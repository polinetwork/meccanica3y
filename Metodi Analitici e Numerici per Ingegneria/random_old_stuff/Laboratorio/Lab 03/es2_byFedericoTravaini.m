%% Esercizio 2
clear; clc;
%% Punto 1
A = 9*eye(7) ...
  - 3*diag(ones(6, 1), -1) ...
  - 3*diag(ones(6, 1),  1) ...
  +   diag(ones(5, 1), -2) ...
  +   diag(ones(5, 1),  2);
b = [ 7; 4; 5; 5; 5; 4; 7 ];
%% Punto 2
if (A == transpose(A))
    disp('Simmetrica');
else
    disp('Non Simmetrica');
end
D = eig(A);
if (D > 0)
    disp('Definita Positiva');
else
    disp('Non Definita Positiva');
end
%% Punto 3
x0 = zeros(7, 1);
nmax = 400;
x = gradiente (A, b, x0, nmax, 1e-6);
%% Punto 4
TOLL = 10.^[-3:-1:-10];
ITER = zeros(size(TOLL));
for i = 1:length(TOLL)
    [x, niter] = gradiente (A, b, x0, nmax, TOLL(i)); 
    ITER(i) = niter;
end
semilogx(TOLL, ITER,'bo-');
grid on;
xlabel ('Tolleranza');
ylabel ('Iterazioni');
