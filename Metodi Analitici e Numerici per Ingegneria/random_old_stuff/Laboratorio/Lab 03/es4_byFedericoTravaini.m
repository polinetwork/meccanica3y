%% Esercizio 4
clear; clc;
%% Punto 1
n = 16;
A = - 2 * diag(ones(n, 1)) ...
  + diag(ones(n-1, 1),  1) ...
  + diag(ones(n-1, 1), -1);
b = [0; ones(n-2, 1); 0 ];
%% Punto 2
[T, iter] = iterqr(A, 1000, 1e-8);
D = diag(T)  % e' definita negativa
%% Punto 3
xcg = pcg (A, b); 
% non funziona perche' e' def. negativa!
% ma -A e' def. positiva, quindi risolvo -Ax = -b
xcg = pcg (-A, -b);
%% Punto 4
x = gradiente(A, b, zeros(n, 1), 10000, 1e-8);
x = gradiente(-A, -b, zeros(n, 1), 10000, 1e-8);
% qui convergono entrambi, perche' l'importante e'
% che la matrice sia definita (pos. o neg.)