clc; clear all; close all;
y0 = 1;
L  = -2;
f = @(t,y) L*y ;
df = @(t,y) L;
u_ex = @(t) y0*exp(L*t);

T = 10;
t_dis=linspace(0,T,1000);

for h = [0.1 0.9 1.1]
[t1,u1] = eulero_avanti(f,T,y0,h);
[t2,u2] = eulero_indietro(f,df,T,y0,h);
figure; 
plot(t1,u1,'bo-',t2,u2,'ro-',t_dis,u_ex(t_dis),'k')
title(strcat('\lambda = -2  h =',num2str(h)))
legend('Eulero Avanti','Eulero Indietro', 'y(t)')
end
