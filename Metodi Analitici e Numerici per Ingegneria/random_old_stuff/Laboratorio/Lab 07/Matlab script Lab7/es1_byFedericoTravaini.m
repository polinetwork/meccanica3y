%%
clc; clear all; close all;

u_ex = @(t) sin(t).*exp(-t/2);
f = @(t,y) cos(t).*exp(-t/2) - 0.5*y ;
df = @(t,y) -0.5;
h = 0.5;
T = 10;
t_dis=linspace(0,T,1000);

[t1,u1] = eulero_avanti(f,T,0,h);
[t2,u2,I2] = eulero_indietro(f,df,T,0,h);
[t3,u3,I3] = crank_nicolson(f,df,T,0,h);

figure(1); plot(t1,u1,'bo-',t2,u2,'ro-',t3,u3,'go-',t_dis,u_ex(t_dis),'k')
legend('Eulero avanti','Eulero indietro', 'Crank Nicolson', 'y(t)')
%% punto 3 
H = [0.4 0.2 0.1 0.05 0.025 0.0125];
E1 = [];
E2 = [];
E3 = [];
for h = H
    [t1,u1] = eulero_avanti(f,T,0,h);
    E1 = [E1 max(abs(u1-u_ex(t1)))];
    
    [t2,u2] = eulero_indietro(f,df,T,0,h);
    E2 = [E2 max(abs(u2-u_ex(t2)))];
    
    [t3,u3] = crank_nicolson(f,df,T,0,h);
    E3 = [E3 max(abs(u3-u_ex(t3)))];
end
figure
loglog(H,E1,'bo-',H,E2,'ro-',H,E3,'go-',H,H,'k',H,H.^2,'k--')
legend('Eulero avanti','Eulero indietro', 'Crank Nicolson', 'h', 'h^2')

   