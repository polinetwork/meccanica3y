%% Esercizio 1
clear; clc;
%% Punto 1
A = [ -10, 0, 1; 0, 2, 3; 1, 3, 1 ];
% Prop: l'algoritmo LU senza pivoting e' ben definito sse i minori
% sulla diagonale della matrice sono diversi da 0.
% cof(A) = det(A) * A^{-T}
disp(diag(inv(A)'*det(A)));
% ... non sono nulli, quindi la condizione e' verificata.
% Una condizione sufficiente (ma non necessaria) e' che sia a dominanza
% diagonale:
disp(abs(diag(A)) > abs(sum(A - diag(diag(A)), 2)));
% ... non lo e'.
%% Punto 2
[L, U, P] = lu(A);
disp(P);
% e' stato effettuato il pivoting, infatti la matrice non e' a dominanza
% diagonale.
% Attenzione: Matlab utilizza sempre il pivoting, quindi la fattorizzazione
% LU esiste sempre, anche se la Proposizione del punto 1 non e' verificata!
% Esempio:
% A = [ -10, 0, 1; 0, 2, 3; 0, 3, 0 ];
% in questo caso non esiste la fattorizzazione senza pivoting, ma esiste
% quella con il pivoting.
%% Punto 3
xex = [1, 1, 1]';
b = A * xex;
x = bksub(U, fwsub(L, P*b));
err = norm(x - xex) / norm(xex);
res = norm(A*x - b) / norm(b);
%% Punto 4
disp(err);
disp(res);
