%% Esercizio 2
clear; clc;
%% Punto 1
a  = -1; b = 8;
f  = @(x) (x-1).^2 .* cos(pi/2*x);
df = @(x) (x-1) .* ( 2*cos(pi/2*x) - pi/2*(x-1).*sin(pi/2*x) );
x = linspace(a, b, 1000);
plot(x, [f(x); df(x)], 'LineWidth', 2.0); 
grid on; hold on;
% Radici = { 1, 3, 5, 7 }
plot([1, 3, 5, 7], zeros(1, 4), '.k', 'MarkerSize', 20);
legend('f(x)', 'f''(x)', 'Radici');
%% Punto 2
x0 = 0.5;
fprintf('Caso x0 = %f\n', x0);
[xvect1, it1] = newton(x0, 100, 1e-10, f, df, 1);
x0 = 4.0;
fprintf('Caso x0 = %f\n', x0);
% osservare che non sempre newton converge verso lo zero piu' vicino!
[xvect2, it2] = newton(x0, 100, 1e-10, f, df, 1);
figure();
semilogy(1:it1, abs(xvect1-1.0), 1:it2, abs(xvect2-7.0), ...
  'LineWidth', 2.0);
grid on;
legend('x0 = 0.5', 'x0 = 4.0');
%% Punto 3
% utilizzando Newton semplice (m=1), la prima radice viene approssimata
% con ordine 1, perche' la molteplicita' e' > 1 (e' 3).
% la seconda radice e' semplice, quindi approssimazione quadratica
%% Punto 4
stimap(xvect1);
stimap(xvect2);
