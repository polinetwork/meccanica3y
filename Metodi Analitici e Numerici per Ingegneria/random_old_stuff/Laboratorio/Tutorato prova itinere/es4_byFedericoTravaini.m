%% Esercizio 4
clear; clc;
%% Punto 1
f = @(x) x.^2 .* sin(x);
a = 0; b = 2*pi;
ezplot(f, [a, b]); grid on;
%% Punto 2
K = 4 : 4 : 20;
I = zeros(length(K), 3);
for i = 1:length(K)
  n = K(i);
  I(i, 1) = pmedcomp(a, b, n, f);
  I(i, 2) = trapcomp(a, b, n, f);
  I(i, 3) = simpcomp(a, b, n, f);
end
figure();
plot(K, I, '.-', 'MarkerSize', 20, 'LineWidth', 2.0); 
title('Integrale');
grid on;
legend('Punto Medio', 'Trapezi', 'Simpson');
%% Punto 3
Iex = - 4*pi^2;
E = abs(I - Iex);
figure();
loglog(K, E, '.-', 'MarkerSize', 20, 'LineWidth', 2.0);
title('Errore');
grid on; hold on;
loglog(K, ((b-a)./K).^2, 'k-', 'LineWidth', 2.0);
loglog(K, ((b-a)./K).^4, 'k--', 'LineWidth', 2.0);
legend('Punto Medio', 'Trapezi', 'Simpson', 'H^2', 'H^4');
% Stima numerica dell'ordine (facoltativo!)
% cerco la retta che meglio approssima il grafico in scala loglog
h = (b-a)./K;
c = polyfit(log(h), log(E(:, 1)'), 1);
fprintf('Ordine punto medio: %d\n', c(1));
c = polyfit(log(h), log(E(:, 2)'), 1);
fprintf('Ordine trapezi: %d\n', c(1));
c = polyfit(log(h), log(E(:, 3)'), 1);
fprintf('Ordine simpson: %d\n', c(1));
