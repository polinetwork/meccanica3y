clear all; close all
N = 10;
n = 1:N;

%************ DST *************

f = @(x) sin(x) + 0.5*sin(2*x) + 0.2*sin(5*x);
%f = @(x) abs(x-pi/2) < pi/4;

% calcolo coefficienti bk tramite dst
xs = pi*n/(N+1);
bk = 2/(N+1)*dst(f(xs));

% plotto i coefficienti bk
figure(1)
plot(n,bk,'o-')

%ricostruisco la funzione
xdis = linspace(0,pi,1000);
fric=0;
for i=n
    fric=fric+bk(i)*sin(i*xdis);
end

%confronto
figure(2)

plot(xdis,f(xdis),'ko', xdis, fric, 'r'); 
legend('f(x)','fric')

%%

%************ DCT *************

g = @(x) cos(x)+3*cos(2*x)+2;

% calcolo coefficienti ak tramite dct
xc=  pi*(2*n - 1)/(2*N);
vec_dct=dct(g(xc));

a0=2*sqrt(1/N)*vec_dct(1);
ak=sqrt(2/N)*vec_dct(2:end);

ak=[a0 ak];

% plotto i coefficienti ak
figure(1)
plot(n-1,ak,'o-')

%ricostruisco la funzione
xdis = linspace(0,pi,1000);
gric=0;
for i=2:N
    gric=gric+ak(i)*cos((i-1)*xdis);
end
gric=gric+ak(1)/2;

%confronto
figure(2)
plot(xdis,g(xdis),'ko', xdis, gric, 'r'); 
legend('g(x)','gric')

%%
%Funzione da scomporre in pari + dispari
h = @(x) sin(x) + 0.5*cos(2*x);

hp= @(x) 0.5*(h(x) + h(-x));
hd= @(x) 0.5*(h(x) - h(-x));

% calcolo coefficienti bk tramite dst
xs = pi*n/(N+1);
bk = 2/(N+1)*dst(hd(xs));

% calcolo coefficienti ak tramite dct
xc=  pi*(2*n - 1)/(2*N);
vec_dct=dct(hp(xc));

a0=2*sqrt(1/N)*vec_dct(1);
ak=sqrt(2/N)*vec_dct(2:end);

ak=[a0 ak];

%plot
figure(1)
plot(n,bk,'o-')
legend('bk')
figure(2)
plot(n-1,ak,'o-')
legend('ak')
%ricostruisco la funzione
xdis = linspace(0,pi,1000);
hric=0;
for i=2:N
    hric=hric+ak(i)*cos((i-1)*xdis);
end
for i=n
    hric=hric+bk(i)*sin(i*xdis);
end

hric=hric+ak(1)/2;


figure(3)
plot(xdis,h(xdis),'ko', xdis, hric, 'r');
legend('h(x)','hric')

%%
%Funzione qualsiasi da prolungare pari o dispari:
close all
clear all; close all
N = 10;
n = 1:N;

w = @(x) x;

%Prolumgamento dispari = utilizzo dst

% calcolo coefficienti bk tramite dst
xs = pi*n/(N+1);
bk = 2/(N+1)*dst(w(xs));

% plotto i coefficienti bk
figure(1)
plot(n,bk,'o-')
legend('bk')

%ricostruisco la funzione
xdis = linspace(0,pi,1000);
wricd=0;
figure(2)
for i=n
    wricd=wricd+bk(i)*sin(i*xdis);
    plot( xdis, wricd)
    hold on
end
hold on
%confronto
plot(xdis,w(xdis),'r'); 

%Prolungo pari = utilizzo dct

% calcolo coefficienti ak tramite dct
xc=  pi*(2*n - 1)/(2*N);
vec_dct=dct(w(xc));

a0=2*sqrt(1/N)*vec_dct(1);
ak=sqrt(2/N)*vec_dct(2:end);

ak=[a0 ak];

% plotto i coefficienti ak
figure(3)
plot(n-1,ak,'o-')
legend('ak')

%ricostruisco la funzione
xdis = linspace(0,pi,1000);
wricp=0;
figure(4)
for i=2:N
    wricp=wricp+ak(i)*cos((i-1)*xdis);
    plot(xdis, ak(1)/2+wricp)
    hold on
end
wricp=wricp+ak(1)/2;

%confronto
hold on
plot(xdis,w(xdis),'r');
