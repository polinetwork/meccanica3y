clear all; close all; clc;

f = @(t,u,v) 4 - u;

h = pi/4;
tmax = 3/2*pi;
t = 0:h:tmax;

u(1) = 0;
v(1) = -1;

for i = 1:length(t)-1
    u(i+1) = u(i) + h*v(i) + h^2/2*(4 - u(i));
    v(i+1) = v(i) + h/2*(4 - u(i) + 4 - u(i+1));
end

lag = @(t) 4 - 4.*cos(t) - sin(t);

plot(t,u,'og-',t,lag(t),'ro-')
legend('leapfrog','lagrange')

H = [pi/4,pi/8,pi/16,pi/32];

yex = 5;

for j = 1:4;
    h = H(j);
    t = 0:h:tmax;
    for i = 1:length(t)-1
        u(i+1) = u(i) + h*v(i) + h^2/2*(4 - u(i));
        v(i+1) = v(i) + h/2*(4 - u(i) + 4 - u(i+1));
    end
    err(j) = max(abs(yex-u(end)));
end

figure()
loglog(H,err,'g',H,H.^2,'k--')
legend('leapfrog','H^2')