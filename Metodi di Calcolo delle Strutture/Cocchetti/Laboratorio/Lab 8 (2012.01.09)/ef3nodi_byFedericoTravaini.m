%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                            retic_2D_St.m                                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                         %
%                Analisi statica di strutture reticolari 2D               %
%                                                                         %
%                  Giuseppe COCCHETTI  e  Aram CORNAGGIA                  %
%                                                                         %
%                          versione del 07/01/2013                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%   Il presente software � prodotto ed � da utilizzarsi per finalit�      %
%   di tipo esclusivamente didattico.                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

clear all
clc


%%% Fase di input %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% matrice delle coordinate
dXY=[   0,    0;
     1000,    0;
     1000, 1000;
        0, 1000];
    
nNodi = size(dXY,1);

% matrice delle incidenze
nInc=[1,2,4;
      2,3,4];

nInc=[nInc,[nInc(:,1)*2-1, nInc(:,1)*2, nInc(:,2)*2-1, nInc(:,2)*2, nInc(:,3)*2-1, nInc(:,3)*2]];

nElem=size(nInc,1);

% matrici costitutive

dE=206000*ones([nElem,1]); % N/mm^2

dni=0.3*ones([nElem,1]); % []

% vettore degli spessori
dt=10*ones([nElem,1]);

% calcolo dei gradi di libert�

nNgdlTot=2*nNodi;

% assegnazione forze nodali
dT=zeros(nNgdlTot,1);
dT(3,1)=200*dt(2,1)*1000/2; % N
dT(5,1)=200*dt(2,1)*1000/2; % N

% definizione gdl vincolati
nNgdlVinc=[1,2,4,7];

% spostamenti assegnati
dus=[0;
     0;
     0;
     0];

%%% Fase di elaborazione %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

dA=zeros([nElem,1]);
dK=zeros([nNgdlTot,nNgdlTot]);
du=zeros([nNgdlTot,1]);
dS=zeros([nNgdlTot,1]);

for ne=1:nElem
    
    % nodi dell'elemento finito n-esimo
    nA=nInc(ne,1);
    nB=nInc(ne,2);
    nC=nInc(ne,3);
    dxA=dXY(nA,1);
    dxB=dXY(nB,1);
    dxC=dXY(nC,1);
    dyA=dXY(nA,2);
    dyB=dXY(nB,2);
    dyC=dXY(nC,2);
    
    % matrice jacobiana
    dJ=[dxA, dyA, 1;
        dxB, dyB, 1;
        dxC, dyC, 1];
    
    % area dell'asta ne-esima
    dA(ne,1)=abs(det(dJ))/2;
    
    % matrice di compatibilit�
    dBne=[dyB-dyC,       0, dyC-dyA,       0, dyA-dyB,       0;
                0, dxC-dxB,       0, dxA-dxC,       0, dxB-dxA;
          dxC-dxB, dyB-dyC, dxA-dxC, dyC-dyA, dxB-dxA, dyA-dyB]/det(dJ);
    
    % matrice costitutiva dell'elemento finito ne-esimo
    dEne=dE(ne,1)/(1-dni(ne,1)^2)*[        1, dni(ne,1),               0;
                                     dni(ne,1),         1,               0;
                                             0,         0, (1-dni(ne,1))/2];
      
      
      
    dke=dt(ne,1)*dA(ne,1)*dBne'*dEne*dBne;
    
    % assemblaggio della matrice di rigidezza dell'asta ne-esima
    v=nInc(ne,4:9);
    dK(v,v)=dK(v,v)+dke; 
    
end
    
 
% definizione gdl liberi
nNgdlLib=[1:nNgdlTot];
nNgdlLib(nNgdlVinc)=[];

%%% Risoluzione del sistema

% partizione matrici e vettori
dKuu=dK(nNgdlLib,nNgdlLib);
dKss=dK(nNgdlVinc,nNgdlVinc);
dKus=dK(nNgdlLib,nNgdlVinc);
dKsu=dK(nNgdlVinc,nNgdlLib);

dTu=dT(nNgdlLib,1);
dTs=dT(nNgdlVinc,1);

% risoluzione
duu=dKuu\(dTu-dKus*dus);
dSs=dKsu*duu+dKss*dus-dTs;


% Ri-ordinamento
% spostamenti
du(nNgdlLib,1)=duu;
du(nNgdlVinc,1)=dus;
du

% reazioni vincolari
dS(nNgdlVinc,1)=dSs;
dS



% routine grafica per plottaggio deformata

% definizione parametri per la rappresentazione grafica
dxMin=min(dXY(:,1));
dxMax=max(dXY(:,1));
ddx=dxMax-dxMin;
dxMin=dxMin-ddx/10;
dxMax=dxMax+ddx/10;

dyMin=min(dXY(:,2));
dyMax=max(dXY(:,2));
ddy=dyMax-dyMin;
dyMin=dyMin-ddy/10;
dyMax=dyMax+ddy/10;


figure(1)
title('Configurazione deformata')
axis([dxMin,dxMax,dyMin,dyMax]*1.1)
axis equal
hold on

dAmplif=(ddx/10)/max(abs(du));
for ne=1:nElem
    % nodi dell'elemento finito ne-esimo
    nA=nInc(ne,1);
    nB=nInc(ne,2);
    nC=nInc(ne,3);

    % coordinate dei nodi dell'elemento finito ne-esimo
    dxA=dXY(nA,1);
    dyA=dXY(nA,2);
    dxB=dXY(nB,1);
    dyB=dXY(nB,2);
    dxC=dXY(nC,1);
    dyC=dXY(nC,2);
  
    plot([dxA,dxB,dxC,dxA],[dyA,dyB,dyC,dyA],'k-')

    
    % spostamenti dei nodi dell'elemento finito ne-esimo
    v=nInc(ne,4:9);
    dune=du(v,1);
    
    dudef=dune([1,3,5],1)*dAmplif;
    dvdef=dune([2,4,6],1)*dAmplif;
    plot([dxA+dudef(1),dxB+dudef(2),dxC+dudef(3),dxA+dudef(1)],[dyA+dvdef(1),dyB+dvdef(2),dyC+dvdef(3),dyA+dvdef(1)],'b-')
end


